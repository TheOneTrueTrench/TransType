using System;

namespace TransType
{
    public class DotNetConfig
    {
        public NamingStrategy ClassNaming { get; set; }
        public NamingStrategy MemberNaming { get; set; }
        public string Namespace { get; set; }
        public string OutputDirectory { get; set; }
        public DotNetConfig()
        {

        }
        public DotNetConfig(Type type)
        {
            Namespace = type.Namespace;
            ClassNaming = NamingStrategy.Pascal;
            MemberNaming = NamingStrategy.Pascal;
            OutputDirectory = "cs";
        }
    }
    
}
