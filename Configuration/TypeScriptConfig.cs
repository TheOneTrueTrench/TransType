namespace TransType
{
    public class TypeScriptConfig 
    {
        public NamingStrategy ClassNaming { get; set; }
        public NamingStrategy MemberNaming { get; set; }
        
        public string OutputDirectory { get; set; }
    }
    
}
