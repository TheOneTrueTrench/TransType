﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransType.ImportProcessing
{
    public class DotNetTypeImportProcessing : TypeImportProcessing
    {
        public DotNetTypeImportProcessing(Type input)
        {
            Input = input;
        }

        protected Type Input { get; set; }
    }
}
