using System;
using System.Runtime.Serialization;

namespace TransType
{
    [Serializable]
    internal class InvalidTypeNameException : Exception
    {

        public InvalidTypeNameException(string typeName) : base("Invalid Type name: " + typeName)
        {
        }

        public InvalidTypeNameException(string typeName, Exception innerException) : base("Invalid Type name: " + typeName, innerException)
        {
        }

        protected InvalidTypeNameException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}