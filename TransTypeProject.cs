using System.Collections.Generic;

namespace TransType
{
    public class TransTypeProject
    {
        public string ProjectName { get; set; }
        public DotNetConfig DotNet { get; set; }
        public TypeScriptConfig TypeScript { get; set; }
        public Dictionary<string, Dictionary<string, string>> Types { get; set; }
    }
    
}
