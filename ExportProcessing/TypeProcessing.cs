using System.Collections.Generic;

namespace TransType
{
    public abstract class TypeProcessing
    {
        public TypeProcessing(KeyValuePair<string, Dictionary<string, string>> typeInfo)
        {
            TypeInfo = typeInfo;
        }

        protected KeyValuePair<string, Dictionary<string, string>> TypeInfo { get; }

        public abstract void GenerateTypeFile();
    }
}
