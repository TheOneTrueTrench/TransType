using System.Collections.Generic;

namespace TransType
{
    public abstract class MemberProcessing
    {
        public MemberProcessing(KeyValuePair<string, string> member)
        {
            Member = member;
        }

        protected KeyValuePair<string, string> Member { get; }

        public abstract string MemberOutput();

        protected TypeData CreateTypeDescription(string typeName)
        {
            if (typeName.EndsWith("<>")) // Dictionary
            {
                return new StringMap{
                    InternalType = CreateTypeDescription(typeName.Substring(0, typeName.Length - 2))
                };
                
            }
            if (typeName.EndsWith("[]")) // Collection/Array
            {
                return new Collection{
                    InternalType = CreateTypeDescription(typeName.Substring(0, typeName.Length - 2))
                };
            }
            return new DirectType{
                Name = typeName
            };
        }

        
    }
}
