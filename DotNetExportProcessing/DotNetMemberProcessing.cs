using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TransType
{
    public class DotNetMemberProcessing : MemberProcessing
    {
        private DotNetConfig Config { get; }

        public static string TypeName(Type input) {
            if (TypeOverrides.ContainsKey(input.Name)){
                return TypeIsoNames.Single(t => t.Key == input.Name).Key;
            }
            else return input.Name;
        }

        public DotNetMemberProcessing(KeyValuePair<string, string> member, DotNetConfig config) : base(member)
        {
            Config = config;
        }

        private static Dictionary<string, string> TypeOverrides = new Dictionary<string, string>
        {
            {"uint8", "byte"},
            {"int8", "sbyte"},
            {"uint16", "ushort"},
            {"int16", "short"},
            {"uint", "uint"},
            {"int", "int"},
            {"uint64", "ulong"},
            {"int64", "long"},
            {"string", "string"},
            {"decimal", "decimal"},
            {"float", "float"},
            {"double", "double"},
        };
        private static Dictionary<string, string> TypeIsoNames = new Dictionary<string, string>
        {
            {"Byte", "uint8"},
            {"SByte", "int8"},
            {"UInt16", "uint16"},
            {"Int16", "int16"},
            {"UInt32", "uint"},
            {"Int32", "int"},
            {"UInt64", "uint64"},
            {"Int64", "int64"},
            {"String", "string"},
            {"Decimal", "decimal"},
            {"Single", "float"},
            {"Double", "double"},
        };
        
        private string indent => "\t\t";
        private string TypeNameOutput(TypeData typeData) 
        {
            if (typeData is StringMap map)
            {
                return "Dictionary<string, " + TypeNameOutput(map.InternalType) + ">";
            }
            if (typeData is Collection col)
            {
                return "IEnumerable<" + TypeNameOutput(col.InternalType) + ">";
            }
            if (typeData is DirectType typ)
            {
                if (TypeOverrides.ContainsKey(typ.Name))
                {
                    return TypeOverrides[typ.Name];
                }
                // We're dealing with the actual base type now. It should be an actual type name.
                // First, let's make sure this is strictly AlphaNumeric
                if (!Regex.Match(typ.Name, "^[A-Za-z0-9]+$").Success)
                {
                    throw new InvalidTypeNameException(typ.Name);
                }
                // Split it up by spaces.
                var words = typ.Name.Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
                if (words.Length < 1)
                {
                    throw new InvalidTypeNameException(typ.Name);
                }
                
                var output = words.First();
                if (Config.ClassNaming == NamingStrategy.Camel)
                {
                    output = output.LowerFirstLetter();
                }
                else 
                {
                    output = output.UpperFirstLetter();
                }
                foreach (var word in words.Skip(1))
                {
                    output += word.UpperFirstLetter();
                }
                return output;
            }
            throw new InvalidTypeNameException("unknown error");
        }
    
        public override string MemberOutput() => indent + $"public {TypeNameOutput(base.CreateTypeDescription(this.Member.Value))} {NameOutput(Member.Key)} {{ get; set; }}";

        private string NameOutput(string memberName)
        {
            var words = memberName.Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
            if (words.Length < 1)
            {
                throw new InvalidTypeNameException(memberName);
            }
            
            var output = words.First();
            if (Config.MemberNaming == NamingStrategy.Camel)
            {
                output = output.LowerFirstLetter();
            }
            else 
            {
                output = output.UpperFirstLetter();
            }
            foreach (var word in words.Skip(1))
            {
                output += word.UpperFirstLetter();
            }
            return output;
        }
    }
}
