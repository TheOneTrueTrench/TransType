using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace TransType
{
    public class DotNetTypeProcessing : TypeProcessing
    {
        private string endl => Environment.NewLine;
        private DotNetConfig Config { get; }
        private FileInfo ConfigFile { get; }

        public DotNetTypeProcessing(KeyValuePair<string, Dictionary<string, string>> member, DotNetConfig config, FileInfo configFile) : base(member)
        {
            Config = config;
            ConfigFile = configFile;
        }
        private string TypeNameOutput(string typeName) 
        {
            if (!Regex.Match(typeName, "^[A-Za-z0-9 ]+$").Success)
            {
                throw new InvalidTypeNameException(typeName);
            }
            // Split it up by spaces.
            var words = typeName.Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
            if (words.Length < 1)
            {
                throw new InvalidTypeNameException(typeName);
            }
            
            var output = words.First();
            if (Config.ClassNaming == NamingStrategy.Camel)
            {
                output = output.LowerFirstLetter();
            }
            else 
            {
                output = output.UpperFirstLetter();
            }
            foreach (var word in words.Skip(1))
            {
                output += word.UpperFirstLetter();
            }
            return output;
        }
        
        private string indent => "\t";

        public string TypeOutput()
        {
            var output = "";
            output += "using System.Collections.Generic;" + endl;
            output += $"namespace {Config.Namespace}" + endl;
            output += "{" + endl;
            output += indent + $"public class {TypeNameOutput(this.TypeInfo.Key)}" + endl;
            output += indent +"{" + endl;
            foreach (var member in TypeInfo.Value)
            {
                output += new DotNetMemberProcessing(member, Config).MemberOutput() + endl;
            }
            output += indent + "}" + endl;
            output += "}" + endl;
            return output;
        } 

        public override void GenerateTypeFile()
        {
            var contents = TypeOutput();
            
            DirectoryInfo outputDir = new DirectoryInfo(ConfigFile.DirectoryName + "\\" + Config.OutputDirectory);
            if (!outputDir.Exists)
            {
                outputDir.Create();
            }
            File.WriteAllText(outputDir.FullName + "\\" + TypeNameOutput(TypeInfo.Key) + ".cs", contents);
        }
    }
}
