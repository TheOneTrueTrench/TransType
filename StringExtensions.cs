namespace TransType
{
    public static class StringExtensions 
    {
        public static string LowerFirstLetter(this string input) => input.Substring(0,1).ToLower() + input.Substring(1);
        public static string UpperFirstLetter(this string input) => input.Substring(0,1).ToUpper() + input.Substring(1);
    }
}
