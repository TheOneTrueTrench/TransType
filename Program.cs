﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace TransType
{
    class Program
    {
        static void Main(string[] args)
        {

            string inputDllPath = args.First();
            var inputDll = Assembly.LoadFile(inputDllPath);
            string selectedNamespace = SelectNamespace(inputDll);
            Type[] selectedTypes = GetNamespaceTypes(inputDll, selectedNamespace);

            

            var inputData = File.ReadAllText(args.First());
            var outputObj = new TransTypeProject{
                 DotNet = new DotNetConfig
                 {
                     ClassNaming = NamingStrategy.Pascal,
                     MemberNaming = NamingStrategy.Pascal,
                     Namespace = selectedNamespace,
                     OutputDirectory = "cs"
                 },
                 TypeScript = new TypeScriptConfig
                 {
                     ClassNaming = NamingStrategy.Camel,
                     MemberNaming = NamingStrategy.Camel,
                     OutputDirectory = "ts"
                 },
                 ProjectName = selectedNamespace,
                 Types = selectedTypes.ToDictionary(
                    t => GenerateSpaceName(t.Name),
                    t => t.GetProperties()
                    .ToDictionary(
                        m => GenerateSpaceName(m.Name),
                        m => DotNetMemberProcessing.TypeName(m.GetType())
                    )
                 )
             };
            Console.WriteLine(
                JsonConvert.SerializeObject(outputObj, new JsonSerializerSettings{
                    Formatting = Formatting.Indented,
                    ContractResolver = new DefaultContractResolver{
                        NamingStrategy = new DefaultNamingStrategy()
                    },
                    Converters = new List<JsonConverter>{
                        new StringEnumConverter()
                    }
                })
            );
        }
        private static string GenerateSpaceName(string input) {
            return input;
        }

        private static Type[] GetNamespaceTypes(Assembly inputDll, string selectedNamespace)
        {
            return inputDll.GetExportedTypes().Where(t => t.Namespace == selectedNamespace).ToArray();
        }

        private static string SelectNamespace(Assembly inputDll)
        {
            var possibleNamespaces = inputDll.GetExportedTypes().Select(t => t.Namespace).Distinct().ToArray();
            Console.Clear();
            Console.Write("Please select which namespace you would like to export: ");
            var (x, y) = (Console.CursorLeft, Console.CursorTop);
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < possibleNamespaces.Count(); i++)
            {
                Console.WriteLine($"\t{i + 1}. {possibleNamespaces[i]}");
            }
            (Console.CursorLeft, Console.CursorTop) = (x, y);
            string res = Console.ReadLine();
            Console.Clear();
            Console.Write("");
            int pick = int.Parse(res);
            return possibleNamespaces[pick-1];
        }

        public static void GenerateOutput(string[] args)
        {
            string configFilePath = args.First();
            var file = new FileInfo(configFilePath);
            string data = File.ReadAllText(file.FullName);
            var project = JsonConvert.DeserializeObject<TransTypeProject>(data,
                new JsonSerializerSettings{
                    Formatting = Formatting.Indented,
                    ContractResolver = new DefaultContractResolver{
                        NamingStrategy = new DefaultNamingStrategy()
                    },
                    Converters = new List<JsonConverter>{
                        new StringEnumConverter()
                    }
                }
            );
            foreach (var type in project.Types)
            {
                new DotNetTypeProcessing(type, project.DotNet, file).GenerateTypeFile();
                // Console.WriteLine(output);
            }
        }

    }
}
